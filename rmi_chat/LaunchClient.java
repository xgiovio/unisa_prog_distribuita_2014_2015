package rmi_chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class LaunchClient {

    public static void main (String[] args ) {

        String name = "default_name"; // or args[0]

        try {
            ClientInterface ci = new Client();
            ChatInterface server = (ChatInterface) Naming.lookup("rmi://localhost/serverchat");
            server.register(ci,name);
            BufferedReader i = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String cmd = command(i);
                if (!cmd.equals("quit")) {
                    server.speak(new Message(name, cmd));
                } else {
                    server.unregister(ci, name);
                    break;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        finally {
            System.exit(0);
        }


    }

    private static String command (BufferedReader i){
        try {
            return i.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "quit";
    }




}
