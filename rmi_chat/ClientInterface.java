package rmi_chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 09/12/2014.
 */
public interface ClientInterface extends Remote {

    void spoken (Message m) throws RemoteException;
    void bye (String to) throws  RemoteException;
    void welcome (String to) throws RemoteException;

}
