package rmi_chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    public Client () throws RemoteException {

    }

    @Override
    public void spoken(Message m) throws RemoteException {
        System.out.println(m.getFrom() +" : " + m.getM());
    }

    @Override
    public void bye(String to) throws RemoteException {
        System.out.println(to + " has quit");

    }

    @Override
    public void welcome(String to) throws RemoteException {
        System.out.println(to + " has joined");

    }
}
