package rmi_chat;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class LaunchServer {

    public static void main (String[] args ){

        try {
            ChatInterface cs = new ChatServer();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("serverchat",cs);



        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


}
