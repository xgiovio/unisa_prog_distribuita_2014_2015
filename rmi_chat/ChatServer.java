package rmi_chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Giovanni on 09/12/2014.
 */
public class ChatServer extends UnicastRemoteObject implements ChatInterface {

    public ChatServer () throws RemoteException{
       list = new ArrayList<ClientInterface>();
    }


    @Override
    public void speak( Message m) throws RemoteException {
        Iterator<ClientInterface> it = list.iterator();
        for (;it.hasNext();){
            it.next().spoken(m);
        }
    }

    @Override
    public void register(ClientInterface c, String n) throws RemoteException {
        Iterator<ClientInterface> it = list.iterator();
        for (;it.hasNext();){
            it.next().welcome(n);
        }
        list.add(c);
    }

    @Override
    public void unregister(ClientInterface c, String n) throws RemoteException {
        Iterator<ClientInterface> it = list.iterator();
        for (;it.hasNext();){
            it.next().bye(n);
        }
        list.remove(c);
    }


    private ArrayList<ClientInterface> list;
}
