package rmi_chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 09/12/2014.
 */
public interface ChatInterface extends Remote{

    void speak (Message m) throws RemoteException;
    void register (ClientInterface c, String n) throws RemoteException;
    void unregister (ClientInterface c, String n) throws RemoteException;


}
