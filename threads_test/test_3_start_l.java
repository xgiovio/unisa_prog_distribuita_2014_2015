package threads_test;

/**
 * Created by Giovanni on 12/10/2014.
 */
public class test_3_start_l {


    public static void main (String[] args) {

        class Thread1 implements Runnable{


            @Override
            public void run() {
                test_3_static_synchronized_lock.op1();
            }
        }

        class Thread2 implements Runnable{


            @Override
            public void run() {
                test_3_static_synchronized_lock.op2();
            }
        }

        class Thread3 implements Runnable{


            @Override
            public void run() {
                test_3_static_synchronized_lock.op3();
            }
        }

        Thread a =  new Thread(new Thread1());
        a.start();
        Thread b =  new Thread(new Thread2());
        b.start();
        Thread c =  new Thread(new Thread3());
        c.start();

        try {
            a.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            b.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            c.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
