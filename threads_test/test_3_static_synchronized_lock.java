package threads_test;

/**
 * Created by Giovanni on 12/10/2014.
 */
public class test_3_static_synchronized_lock {

    public static  void op1()  {
        try {

            synchronized (test_3_static_synchronized_lock.class) {
                Thread.currentThread().sleep(5000);
                System.out.print("OP1 eseguita");
            }



        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static  void op2()  {
        synchronized (test_3_static_synchronized_lock.class) {
                System.out.print("OP2 eseguita");
        }
    }

    public static void op3()  {
        System.out.print("OP3 eseguita");
    }




}
