package threads_test;

/**
 * Created by Giovanni on 12/10/2014.
 */
public class test_3_static_synchronized_methods {

    public static synchronized void op1()  {
        try {
            Thread.currentThread().sleep(5000);
            System.out.print("OP1 eseguita");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static synchronized void op2()  {
                System.out.print("OP2 eseguita");
    }

    public static void op3()  {
        System.out.print("OP3 eseguita");
    }




}
