package socket_chat_udp;

import java.io.*;
import java.net.*;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 02/12/14
 * Time: 13:54
 */
public class Client {

    private static final Logger l = Logger.getLogger(ClientInputManagement.class.getName());

    public Client (String inome ,String iaddress, int iport, int ittl) {

        address =  iaddress;
        port= iport;
        nome = inome;
        ttl=ittl;

    }

    public void launch (){

        MulticastSocket connection = null;
        DatagramPacket packet = null;

        try {
            connection = new MulticastSocket(port);
            connection.setTimeToLive(ttl);
            connection.joinGroup(InetAddress.getByName(address));

            BufferedReader userinput = new BufferedReader(new InputStreamReader(System.in));

            ClientInputManagement cim = new ClientInputManagement(connection);
            cim.start();

            String message = nome + " si e' connesso";
            packet = new DatagramPacket(message.getBytes(),message.length(),InetAddress.getByName(address),port);
            connection.send(packet);

            String command = command(userinput);
            for (; !("!quit".equals(command)) ;){
                packet = new DatagramPacket(command.getBytes(),command.length(),InetAddress.getByName(address),port);
                connection.send(packet);
                command = command(userinput);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
                connection.close();
        }


    }


    private String command (BufferedReader i){
        try {
            System.out.print(">> ");
            return i.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }



    private final String address;
    private final int port;
    private final String nome;
    private int ttl ;


}
