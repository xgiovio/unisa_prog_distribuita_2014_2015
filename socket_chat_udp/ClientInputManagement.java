package socket_chat_udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 02/12/14
 * Time: 14:04
 */
public class ClientInputManagement extends Thread {

    private static final Logger l = Logger.getLogger(ClientInputManagement.class.getName());

    public ClientInputManagement (MulticastSocket input){

        i=input;
        buffer = new byte[100];
    }


    @Override
    public void run() {
        while (true){
            try {
                DatagramPacket packet =  new DatagramPacket(buffer,buffer.length);
                Arrays.fill(buffer, new Integer(0).byteValue());
                i.receive(packet);
                System.out.println(new String(buffer).trim());
            } catch (IOException e) {
                System.out.println("Bye");
                break;
            }

        }
    }

    private final MulticastSocket i;
    private byte[] buffer;

}
