package socket_test.test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 12:31
 */
public class server {

    private static final Logger log = Logger.getLogger(socket_test.test.server.class.getName());

    public static void main (String[] arg){


        try {
            ServerSocket open = new ServerSocket(60000);
             log.info("Aperto socket di ascolto su porta 60k");
             final HashMap<String,Record> database = new HashMap<String, Record>();

            for(;true;){
                Socket accept = open.accept();
                log.info("Accettata richiesta");
                Thread request_management_thread = new Thread(new server_request_management(accept,database));
                log.info("Richiesta inoltrata a thread");
                request_management_thread.start();
                log.info("Waiting on port 60k");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
