package socket_test.test;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 13:00
 */
public class server_request_management  implements Runnable{


    private static final Logger log = Logger.getLogger(server_request_management.class.getName());
    public server_request_management (Socket in, HashMap<String,Record> in_h){
               sock = in;
               hash = in_h;
    }


    @Override
    public void run() {
         log.info("Gestendo richiesta");
        try {
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            Record r = (Record) in.readObject();
            if (r.getNome()!= null && r.getCognome()!= null){
                synchronized (hash) {
                    hash.put(r.getNome(), r);
                }
                log.info("Aggiungendo " + r.getNome() + " " + r.getCognome() + " al database");
            }


            //operations on socket
            sock.close();
            log.info("Terminata gestione");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
           finally {
            try {
                sock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    final private Socket sock;
    final private HashMap<String,Record> hash;




}
