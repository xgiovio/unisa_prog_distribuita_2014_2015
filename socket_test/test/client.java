package socket_test.test;

import com.sun.corba.se.pept.encoding.OutputObject;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 12:31
 */
public class client {

    private static final Logger log = Logger.getLogger(client.class.getName());


    public static void main (String[] arg){


        try {
            Socket sock = new Socket("localhost", 60000);
             log.info("Connessione a localhost 60000");

            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            Record r = new Record("Mario", "Rossi");
            log.info("inviando Mario Rossi al server");
            out.writeObject(r);
            sock.close();



        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
