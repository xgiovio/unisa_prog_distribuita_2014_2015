package socket_test.test;

import java.io.Serializable;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 13:20
 */
public class Record implements Serializable {

    private static final long serialVersionUID = 9090L;

    public Record (String in_nome, String in_cognome){

        nome = in_nome;
        cognome = in_cognome;


    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    private String nome;
    private String cognome;
}
