package rmi_exam;

/**
 * Created by Giovanni on 16/01/2015.
 */
public class ClientData {
    public ClientData (String inname){
        name = inname;
    }
    private boolean voto = false;
    private boolean havotato = false;
    private boolean blocked = false;
    private String name;

    public void setVoto(boolean invoto){voto=invoto;}
    public void setHavotato(boolean invotato){havotato = invotato;}
    public void setBlocked (boolean value) {blocked=value;}
    public String getName(){return name;}
    public boolean getvoto() {return voto;};
    public boolean gethavotato(){return havotato;}
    public boolean getblocked(){return blocked;}

}


