package rmi_exam;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Giovanni on 16/01/2015.
 */
public class Server extends UnicastRemoteObject implements ServerInterface {

    private HashMap<ClientInterface,ClientData> h;
    private boolean votazione_in_corso = false;
    private boolean chaton=false;

    public Server() throws RemoteException{
        h=new HashMap<ClientInterface, ClientData>();
    }

    public void iscriviti (ClientInterface c, String n) throws RemoteException {

        h.put(c,new ClientData(n));
        if (h.size()>=2){chaton=true;}
        if (chaton){
            Iterator<ClientInterface> it = h.keySet().iterator();
            for(;it.hasNext();){
                ClientInterface t = it.next();
                t.detto(h.get(c).getName() + " e' entrato in chat");//anche a se stesso
            }

        }
    }

   public void disiscriviti( ClientInterface c) throws RemoteException {
       String name = h.get(c).getName();
       h.remove(c);
       if (chaton){
           Iterator<ClientInterface> it = h.keySet().iterator();
           for (;it.hasNext();){
               ClientInterface t = it.next();
               t.detto(name + " e' uscito dalla chat"); // non a se stesso
           }
       }
   }

    public boolean dici (ClientInterface c, String m) throws  RemoteException {
        if (chaton && !h.get(c).getblocked()){
            Iterator<ClientInterface> it = h.keySet().iterator();
            for (;it.hasNext();){
                ClientInterface t = it.next();
                if(!t.equals(c)){
                    t.detto(h.get(c).getName() + ":" + m);
                }
            }
            return true;
        }
        return false;
    }

    public boolean vota (ClientInterface c, boolean v) throws RemoteException{
        if (chaton && votazione_in_corso && !h.get(c).getblocked() && !h.get(c).gethavotato()){
            h.get(c).setHavotato(true);
            h.get(c).setVoto(v);
            System.out.println(h.get(c).getName() + " ha votato " + convert(v));
            return true;
        }
        return false;
    }

    protected String convert(boolean in){
        if (in)
            return "si";
        return "no";
    }

    public HashMap<ClientInterface,ClientData> getH(){return h;}
    public void setsondaggio(boolean value) {votazione_in_corso=value;}


}
