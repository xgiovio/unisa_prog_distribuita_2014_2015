Struttura del compito\esame invariata. Fixato solo alcuni bug grafici ed errori di sintassi.
Ogni errore riportato qui sotto fa riferimento ad una sola linea di codice del documento consegnato all'esame.
Non ho implementato lock o semafori poiche' avrebbero richiesto una modifica del codice aggiuntiva, quella effettuata è la minima per una giusta visualizzazione.
Tutti gli errori commessi sarebbero stati facilmente trovati mediante una ide o una seconda lettura, cosa che non avuto modo di fare al termine dell'esame.

Bug grafici:
1)sostituito 'int start = comando.indexof("sondaggio") + 2' con 'int start = 10' in LaunchServer
2)sostituito 't.detto(h.get(t).getName() + "e' entrato in chat")' con 't.detto(h.get(c).getName() + "e' entrato in chat")' nel metodo iscriviti di Server
3)aggiunto 'String name = h.get(c).getName();' al metodo disiscriviti di Server
4)sostituito 't.detto(h.get(t).getName() + "e' uscito dalla chat")' con 't.detto(name + " e' uscito dalla chat");' nel metodo disicriviti di Server
5)sostituito 't.detto(h.get(t).getName() + ":" + m)' con 't.detto(h.get(c).getName() + ":" + m)' nel metodo dici di Server
6)sostituito 'System.out.println("Non puoi votare poichè non c'è una votazione in corso,sei stato bloccato o il tuo input è scorretto")' con 'System.out.println("Non puoi votare poichè non c'è una votazione in corso, hai gia votato, sei stato bloccato o il tuo input è scorretto");' nel while di LaunchClient
7)sostituito "t!=c" con "!t.equals(c)" in Server

Errori di sintassi:
1)mancano tutti gli import
2)mancano le parentesi tonde nei getter di ClientData
3)mancano le parentesi tonde nel metodo getH di Server
4)manca void al ritorno del metodo setsondaggio in Server
5)manca ";" nell'ultima istruzione del metodo setsondaggio in Server
6)il ; va spostato all'interno della parentesi graffa nel metodo getname di Client
7)"new securitymanager()" in LaunchClient,LaunchServer va come argomento di System.setsecuritymanager e non come assegnazione
8)"command" usato al posto di "comando" in LaunchClient
9)manca println a System.out in LaunchClient
10)definito "ServerInterface s;" al di fuori del try altrimenti non visibile nello scoping di finally in LaunchClient
11)aggiunto try catch nel blocco finally in LaunchClient
12)"t.dici" invece di "t.detto" in LaunchServer
13)parentesi tonda saltata al termine di un if in LaunchServer
14)else mancante in LaunchClient,non avevo usato le parentesi