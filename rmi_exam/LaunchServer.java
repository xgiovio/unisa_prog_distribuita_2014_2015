package rmi_exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Iterator;

/**
 * Created by Giovanni on 16/01/2015.
 */
public class LaunchServer  {

    public static void main (String[] args){

        System.setSecurityManager(new SecurityManager());

        try {
            ServerInterface s = new Server();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("server",s);
            BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
            String comando = command(b);
            while (true){ //supponiamo il server non esca

                if (!comando.startsWith("!"))
                    System.out.println("Parametro errato");
                else{
                    comando = comando.substring(1);
                    if ( comando.split(" ")[0].equals("blocco") ){
                        Iterator<ClientInterface> it = ((Server)s).getH().keySet().iterator();
                        for (;it.hasNext();){
                            ClientInterface t = it.next();
                            if ( ((Server)s).getH().get(t).getName().equals(comando.split(" ")[1] )){
                                ((Server)s).getH().get(t).setBlocked(true);
                                t.detto("bloccato");
                            }
                        }
                    } else if (comando.split(" ")[0].equals("sblocco")){
                        Iterator<ClientInterface> it = ((Server)s).getH().keySet().iterator();
                        for (;it.hasNext();){
                            ClientInterface t = it.next();
                            if ( ((Server)s).getH().get(t).getName().equals(comando.split(" ")[1] )){
                                ((Server)s).getH().get(t).setBlocked(false);
                                t.detto("sbloccato");
                            }
                        }
                    } else if (comando.split(" ")[0].equals("sondaggio")){

                        ((Server)s).setsondaggio(true);
                        int start = 10;
                        String message_sondaggio = comando.substring(start);
                        Iterator<ClientInterface> it = ((Server)s).getH().keySet().iterator();
                        for (;it.hasNext();){
                            it.next().detto(message_sondaggio);
                        }
                    }else {
                        System.out.println("Comando ! non valido");
                    }
                }
                comando = command(b);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        finally {
            System.exit(0);
        }

    }

    public static String command (BufferedReader b){
        try{
            return b.readLine();
        } catch (IOException e) {
            return "!quit";
        }
    }


}
