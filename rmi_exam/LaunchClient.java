package rmi_exam;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 16/01/2015.
 */
public class LaunchClient {

    public static void main (String[] args ){

        if (args.length <2 || args.length > 2)
            System.exit(-1);
        System.setSecurityManager(new SecurityManager());
        ClientInterface c = null;
        ServerInterface s = null;
        try{
            c = new Client(args[0]);
            s = (ServerInterface) Naming.lookup("rmi://" + args[1] + "/server");
            s.iscriviti(c,( (Client)c ).getname()  );
            BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
            String comando = command(b);

            while (!comando.equals("!quit")){
                if (comando.startsWith("!")){
                    boolean vote_result;
                    if (comando.substring(1).equals("si"))
                        vote_result = s.vota(c,true);
                    else if (comando.substring(1).equals("no"))
                        vote_result = s.vota(c,false);
                    else vote_result = false;

                    if (vote_result)
                        System.out.println("votato");
                    else
                    System.out.println("Non puoi votare poichè non c'è una votazione in corso, hai gia votato, sei stato bloccato o il tuo input è scorretto");
                } else {
                    boolean dici_result;
                    dici_result = s.dici(c,comando);
                    if (!dici_result)
                        System.out.println ("Non puoi parlare poichè non c'è il numero minimo di partecipanti o sei stato bloccato");
                }
                comando = command(b);
            }

        }
        catch (RemoteException e){
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                s.disiscriviti(c);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            System.exit(0);

        }

    }

    public static String command (BufferedReader b){
        try{
            return b.readLine();
        } catch (IOException e) {
            return "!quit";
        }
    }
}
