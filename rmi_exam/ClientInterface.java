package rmi_exam;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 16/01/2015.
 */
public interface ClientInterface extends Remote {
    void detto (String s) throws RemoteException;
}
