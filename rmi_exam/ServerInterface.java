package rmi_exam;



import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 16/01/2015.
 */
public interface ServerInterface extends Remote {
    boolean dici (ClientInterface c, String m) throws RemoteException;
    boolean vota (ClientInterface c, boolean v) throws RemoteException;
    void iscriviti (ClientInterface c, String n) throws RemoteException;
    void disiscriviti (ClientInterface c) throws RemoteException;
}
