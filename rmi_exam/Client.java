package rmi_exam;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 16/01/2015.
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    private final String nome;

    public Client (String in_name) throws RemoteException {
        nome = in_name;
    }

    public  void detto (String s) throws RemoteException{
        System.out.println(s);
    }

    public String getname(){return nome;}


}
