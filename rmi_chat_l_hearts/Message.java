package rmi_chat_l_hearts;

import java.io.Serializable;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class Message implements Serializable{

    public Message(String ifrom, String im){
        from = ifrom;
        m = im;
    }

    public String getFrom() {
        return from;
    }

    public String getM() {
        return m;
    }

    private final String from;
    private final String m;

}
