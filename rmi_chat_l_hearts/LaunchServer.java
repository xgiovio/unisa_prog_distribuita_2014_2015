package rmi_chat_l_hearts;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Giovanni on 11/12/2014.
 */
public class LaunchServer{


    public static void main (String[] args){

        try {
            ServerInterface s = new Server();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("server",s);

        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


}
