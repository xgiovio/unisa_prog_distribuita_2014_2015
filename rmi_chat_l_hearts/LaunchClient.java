package rmi_chat_l_hearts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 11/12/2014.
 */
public class LaunchClient {

    private final static String name = "alfio"; //instead of vm args
    private  static String sex; //instead of vm args

    public static void main (String [] args) {

        try {

            if (Math.random() >= 0.5)
                sex = "UOMO";
            else
                sex = "DONNA";

            System.out.println("Sono un " + sex + " e mi chiamo " +name);
            ServerInterface s = (ServerInterface) Naming.lookup("rmi://localhost/server");
            ClientInterface c = new Client(name,sex,s);
            ((Client)c).setCompagno(s.iscriviti(c, ((Client) c).getSex()));

            BufferedReader i = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String cmd = command(i);
                if (!cmd.equals("!quit")) {
                    if ( ((Client)c).getCompagno() != null)
                        ((Client)c).getCompagno().dici(new Message( ((Client)c).getName() , cmd  ));
                    else
                        System.out.println("non hai ancora un partner. Impossibile inviare messaggi");
                } else {
                    if ( ((Client)c).getCompagno() != null) {
                        ((Client) c).getCompagno().quit(((Client) c).getName());
                    }else{
                        s.disiscriviti(c, ((Client) c).getSex());
                    }
                    break;
                }
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        finally {
            System.exit(0);
        }


    }


    private static String command (BufferedReader i){
        try {
            return i.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "!quit";
    }


}
