package rmi_chat_l_hearts;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Created by Giovanni on 11/12/2014.
 */
public class Server extends UnicastRemoteObject implements ServerInterface {

    public Server () throws RemoteException{
       u = new ArrayList<ClientInterface>();
        d = new ArrayList<ClientInterface>();
    }


    @Override
    public ClientInterface iscriviti(ClientInterface myself, String sex) throws RemoteException {

        if (sex.equals(UOMO) && d.size() > 0 && u.size() == 0) {
            ClientInterface donna = d.remove(0);
            donna.compagno(myself);
            System.out.println("L'ultimo uomo entrato ha subito ricevuto una donna");
            return donna;
        }
        if (sex.equals(DONNA) && u.size() > 0 && d.size() == 0) {
            ClientInterface uomo = u.remove(0);
            uomo.compagno(myself);
            System.out.println("L'ultima donna entrata ha subito ricevuto un uomo");
            return uomo;
        }

        if ( sex.equals(UOMO)) {
            u.add(myself);
            System.out.println("Aggiunto un uomo");

        }else {
            d.add(myself);
            System.out.println("Aggiunto una donna");
        }


        if (u.size() > 0 && d.size() > 0){
            ClientInterface uomo = u.remove(0);
            ClientInterface donna = d.remove(0);
            donna.compagno(uomo);
            uomo.compagno(donna);

            System.out.println("Accoppiato un uomo e una donna");
        }
        return  null;
    }

    @Override
    public void disiscriviti(ClientInterface myself, String sex) throws RemoteException {
        if ( sex.equals(UOMO)) {
            u.remove(myself);
            System.out.println("Rimosso un uomo");

        }else {
            d.remove(myself);
            System.out.println("Rimosso una donna");
        }

    }

    private ArrayList<ClientInterface> u;

    private ArrayList<ClientInterface> d;


}
