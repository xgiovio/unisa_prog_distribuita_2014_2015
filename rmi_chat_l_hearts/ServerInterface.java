package rmi_chat_l_hearts;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 11/12/2014.
 */
public interface ServerInterface extends Remote {

    public static final String UOMO = "UOMO";
    public static final String DONNA = "DONNA";

    ClientInterface iscriviti (ClientInterface myself, String sex) throws RemoteException;
    void disiscriviti (ClientInterface myself, String sex) throws RemoteException;

}
