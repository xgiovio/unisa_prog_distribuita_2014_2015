package rmi_chat_l_hearts;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 11/12/2014.
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    public Client( String iname, String isex, ServerInterface is) throws RemoteException {
        name = iname;
        sex = isex;
        s = is;
    }

    @Override
    public void compagno(ClientInterface in) throws RemoteException {
            compagno = in;
            System.out.println("hai un nuovo partner");
    }

    @Override
    public void dici(Message m) throws RemoteException {
            System.out.println(m.getFrom() +": " + m.getM());
    }

    @Override
    public void quit(String name) throws RemoteException {
        System.out.println(name + " non e' piu il tuo  partner");
        compagno = null;
        setCompagno(s.iscriviti( (ClientInterface)this,sex));
    }

    public ClientInterface getCompagno() {
        return compagno;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public void setCompagno(ClientInterface compagno) {
        if (compagno != null) {
            this.compagno = compagno;
            System.out.println("hai un nuovo partner");
        }
    }

    private ClientInterface compagno = null;
    private final String name;
    private final String sex;
    private ServerInterface s;
}
