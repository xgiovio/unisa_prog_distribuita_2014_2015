package socket_chat_tcp;

import java.io.*;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 02/12/14
 * Time: 13:54
 */
public class Client {

    private static final Logger l = Logger.getLogger(ClientInputManagement.class.getName());

    public Client (String inome ,String iaddress, int iport) {

        address =  iaddress;
        port= iport;
        nome = inome;

    }

    public void launch (){

        Socket connection = null;

        try {
            connection = new Socket(address,port);
            DataOutputStream o = new DataOutputStream(new BufferedOutputStream(connection.getOutputStream()));
            DataInputStream i = new DataInputStream(new BufferedInputStream(connection.getInputStream()));
            BufferedReader userinput = new BufferedReader(new InputStreamReader(System.in));

            ClientInputManagement cim = new ClientInputManagement(i);
            cim.start();

            o.writeUTF(nome);
            o.flush();

            String command = command(userinput);
            for (; !("!quit".equals(command)) ;){
                o.writeUTF(command);
                o.flush();
                command = command(userinput);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            try {
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }


    private String command (BufferedReader i){
        try {
            System.out.print(">> ");
            return i.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }



    private final String address;
    private final int port;
    private final String nome;


}
