package socket_chat_tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 02/12/14
 * Time: 17:30
 */
public class Server {

    public Server ( int iport){
          port = iport;
    }


    public void launch (){

        try {
            ServerSocket socket = new ServerSocket(port);

            while (true) {
                Socket accepted = socket.accept();

                ServerClientHandler sch = new ServerClientHandler(accepted);
                sch.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


  private final int port;

}
