package socket_chat_tcp;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 02/12/14
 * Time: 14:04
 */
public class ClientInputManagement extends Thread {

    private static final Logger l = Logger.getLogger(ClientInputManagement.class.getName());

    public ClientInputManagement (DataInputStream input){
          i=input;
    }


    @Override
    public void run() {
        while (true){
            try {
                String ridden = i.readUTF();
                System.out.println(ridden);
            } catch (IOException e) {
                System.out.println("Bye");
                break;
            }

        }
    }

    private final DataInputStream i;

}
