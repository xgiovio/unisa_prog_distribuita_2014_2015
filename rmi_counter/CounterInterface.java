package rmi_counter;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 16/11/2014.
 */
public interface CounterInterface extends Remote{

    int getvalue (String from) throws RemoteException;
    int sum (String from, int value) throws RemoteException;


}
