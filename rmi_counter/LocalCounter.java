package rmi_counter;

import java.rmi.registry.LocateRegistry;

/**
 * Created by Giovanni on 16/11/2014.
 */
public class LocalCounter {

    public LocalCounter(int in){
        value=in;
    }

    public int increment(){
        return (value++);
    }
    

    public int getlocalvalue(){
        return value;
    }


    private int value;

}
