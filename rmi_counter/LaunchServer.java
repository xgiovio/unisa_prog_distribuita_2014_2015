package rmi_counter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 16/11/2014.
 */
public class LaunchServer {
    private static Logger l =Logger.getLogger("counterlogger");

    public static void main (String args[]) {



        try {

            RemoteCounter rc = new RemoteCounter(0);
            Registry r = LocateRegistry.getRegistry();
            l.info("Trying to bind");
            r.rebind("Counter",rc);
            l.info("Binded");

            BufferedReader i = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                l.info("Commands:status,accesses,any other to quit");
                String command = LaunchServer.cmd(i);
                l.info(command);
                if (command.equals("status")) {
                    l.info(rc.getlocalvalue() + "");
                } else if (command.equals("accesses")) {
                    l.info(rc.getaccesses());
                } else {
                    l.info("quitting");
                    break;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public static String cmd (BufferedReader i ){
        try {
            String input = i.readLine();
            return input;

        } catch (IOException e) {
        }
        return "";
    }

    

}
