package rmi_counter;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 16/11/2014.
 */
public class RemoteCounter extends LocalCounter implements CounterInterface {

    private static Logger l = Logger.getLogger("RemoteCounter");

    public RemoteCounter( int in) throws RemoteException{
        super(in);
        UnicastRemoteObject.exportObject(this,5000);
        accesses = new Vector<String>();
    }

    @Override
    public synchronized int getvalue(String from) throws RemoteException {
        accesses.add("Access from " + from + " value " + getlocalvalue());
        return getlocalvalue();
    }

    @Override
    public synchronized int sum(String from, int value) throws RemoteException {
        accesses.add("Access from " + from + " value to add " + value);
        for (int i = 0;i < value ;i++)
            increment();
        return getlocalvalue();
    }

    public String getaccesses (){
        String returned = "";
        for (Enumeration<String> e = accesses.elements(); e.hasMoreElements() ;)
            returned+="\n" +  e.nextElement();
        return returned;
    }

    Vector<String> accesses;

}
