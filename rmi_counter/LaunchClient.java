package rmi_counter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 16/11/2014.
 */
public class LaunchClient {
    private static Logger l =Logger.getLogger("counterclientlogger");

    public static void main (String args[]) {


        try {

            CounterInterface rc = (CounterInterface) Naming.lookup("rmi://localhost/Counter");
            BufferedReader i = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                l.info("Commands:current,sum <value>,any other to quit");
                String command = LaunchClient.cmd(i);
                l.info(command);
                if (command.equals("current")) {
                    l.info(rc.getvalue("Client1") + "");
                } else if ( command.length()>=3 && command.substring(0,3).equals("sum")) {
                    int value = Integer.parseInt(command.substring(4));
                    l.info(String.valueOf(rc.sum( "Client1",value )));
                } else {
                    l.info("quitting");
                    break;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }


    public static String cmd (BufferedReader i ){
        try {
            String input = i.readLine();
            return input;

        } catch (IOException e) {
        }
        return "";
    }

    

}
