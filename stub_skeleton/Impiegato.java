package stub_skeleton;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 26/10/14
 * Time: 15:15
 */
public interface Impiegato {

    String getNome();
    String getCognome();
    double getStipendio();
    double aumentaStipendio(double in_aumento);

}
