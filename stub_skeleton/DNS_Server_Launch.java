package stub_skeleton;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 12:31
 */
public class DNS_Server_Launch {

    private static final int dns_server_port = 60000;
    private static ServerSocket saved_socket = null;

    private static final Logger log = Logger.getLogger(DNS_Server_Launch.class.getName());

    public static void main (String[] arg){


        try {
            ServerSocket open = new ServerSocket(dns_server_port);
            saved_socket = open;
             log.info("Aperto socket di ascolto su porta " + dns_server_port);
             final HashMap<String,DNS_Record> database = new HashMap<String, DNS_Record>();

            for(;true;){
                Socket accept = open.accept();
                log.info("Accettata richiesta");
                Thread request_management_thread = new Thread(new DNS_Server_Request_Management(accept,database));
                log.info("Richiesta inoltrata a thread");
                request_management_thread.start();
                log.info("Waiting on port " + dns_server_port);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally{
            if (saved_socket!=null)
                try {
                    saved_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }



    }


}
