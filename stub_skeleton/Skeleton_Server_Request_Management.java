package stub_skeleton;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 13:00
 */
public class Skeleton_Server_Request_Management implements Runnable{


    private static final Logger log = Logger.getLogger(Skeleton_Server_Request_Management.class.getName());
    public Skeleton_Server_Request_Management(Socket in, HashMap<String, Impiegato_Server_Object> in_h, Impiegato_Server_Object in_s){
               sock = in;
               hash = in_h;
               server_object = in_s;
    }


    @Override
    public void run() {
         log.info("Gestendo richiesta");
        try {
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());

            String request = (String) in.readObject();

            if (request.equals("getNome")){
                      out.writeObject(server_object.getNome());
                      out.flush();

            }else if (request.equals("getCognome")) {
                      out.writeObject(server_object.getCognome());
                out.flush();

            }else if (request.equals("getStipendio")){
                      out.writeDouble(server_object.getStipendio());
                out.flush();


            } else /* aumenta stipendio  */{
                log.info("Aumentando lo stipendio");
                double aumento = in.readDouble();
                double aumentato = server_object.aumentaStipendio(aumento);
                out.writeDouble(aumentato);
                out.flush();
                log.info("Aumentato");

            }


            //operations on socket
            sock.close();
            log.info("Terminata gestione");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                sock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    final private Socket sock;
    final private HashMap<String,Impiegato_Server_Object> hash;
    private Impiegato_Server_Object server_object = null;




}
