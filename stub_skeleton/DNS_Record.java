package stub_skeleton;

import java.io.Serializable;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 13:20
 */
public class DNS_Record implements Serializable {

    private static final long serialVersionUID = 9091L;

    public DNS_Record(String in_nome_server, String in_ip_server, int in_porta){

    host=in_nome_server;
        ip=in_ip_server;
        porta=in_porta;


    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    private String host = null;
    private String ip = null;
    private int porta = 0;
}
