package stub_skeleton;

import socket_test.test.Record;
import socket_test.test.server_request_management;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 26/10/14
 * Time: 15:21
 */
public class Skeleton implements Runnable{

    private final int skeleton_server_port = 61000;
    private final int dns_server_port = 60000;  // change this value if dns server port change

    private static final Logger log = Logger.getLogger(socket_test.test.server.class.getName());

     public Skeleton (Impiegato in_server_object , HashMap<String,Impiegato_Server_Object> in_database){

                server_object = in_server_object;
                database  = in_database;

     }


    @Override
    public void run() {

          contact_DNS();
          wait_connections();

    }

    void contact_DNS (){



        try {
            Socket sock = new Socket("localhost", dns_server_port);   // connessione al server dns
            savednssocket = sock;
            log.info("Connessione a DNS");
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            DNS_Record dns_record =  new DNS_Record("xserver", InetAddress.getLocalHost().getHostAddress(),skeleton_server_port);
            out.writeObject(dns_record);
            log.info("inviando dati al server dns");
            sock.close();


        } catch (IOException e) {
            log.info("Impossibile contattare il server DNS");
            //e.printStackTrace();
        }
        finally {
            try { if (savednssocket != null)
                    savednssocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    void wait_connections(){

        try {
            ServerSocket open = new ServerSocket(skeleton_server_port);
            serversocketsaved = open;
            log.info("Aperto socket di ascolto su porta " + skeleton_server_port);

            for(;true;){
                Socket accept = open.accept();
                log.info("Accettata richiesta");
                Thread request_management_thread = new Thread(new Skeleton_Server_Request_Management(accept,database, (Impiegato_Server_Object) server_object));
                log.info("Richiesta inoltrata a thread");
                request_management_thread.start();
                log.info("Waiting on port " + skeleton_server_port);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if(serversocketsaved != null)
                serversocketsaved.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }


        private  Impiegato server_object = null;
        final private  HashMap<String,Impiegato_Server_Object> database;
        private Socket savednssocket = null;
        private ServerSocket serversocketsaved= null;


}
