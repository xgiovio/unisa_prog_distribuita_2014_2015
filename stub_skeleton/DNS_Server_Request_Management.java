package stub_skeleton;

import socket_test.test.Record;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 13:00
 */
public class DNS_Server_Request_Management implements Runnable{


    private static final Logger log = Logger.getLogger(DNS_Server_Request_Management.class.getName());
    public DNS_Server_Request_Management(Socket in, HashMap<String, DNS_Record> in_h){
               sock = in;
               hash = in_h;
    }


    @Override
    public void run() {
         log.info("Gestendo richiesta");
        try {
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            DNS_Record r = (DNS_Record) in.readObject();
            if (r.getHost()!= null && r.getIp()!= null && r.getPorta()!=0){
                synchronized (hash) {
                    hash.put(r.getHost(), r);
                }
                log.info("Aggiungendo " + r.getHost() + " " + r.getIp() + " " + r.getPorta() + " al database dns");
            } else {

                if (r.getHost() != null && r.getIp()== null){
                         DNS_Record r_out = hash.get(r.getHost());
                         if (r_out == null)
                             r_out = new DNS_Record(null,null,0);
                         out.writeObject(r_out);
                    log.info("Inviando " + r_out.getHost() + " " + r_out.getIp() + " " + r_out.getPorta() + " al client");


                }  else {

                    DNS_Record r_out = new DNS_Record(null,null,0);
                    out.writeObject(r_out);
                    log.info("Inviando " + r_out.getHost() + " " + r_out.getIp() + " " + r_out.getPorta() + " al client");

                }


            }

            //operations on socket
            sock.close();
            log.info("Terminata gestione");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            try {
                sock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    final private Socket sock;
    final private HashMap<String,DNS_Record> hash;




}
