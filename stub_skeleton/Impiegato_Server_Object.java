package stub_skeleton;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 26/10/14
 * Time: 15:14
 */
public class Impiegato_Server_Object implements Impiegato{

    public Impiegato_Server_Object(String in_nome, String in_cognome, double in_aumento){
        nome = in_nome;
        cognome = in_cognome;
        stipendio = in_aumento;

    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public String getCognome() {
        return cognome;
    }

    @Override
    public double getStipendio() {
        return stipendio;
    }

    @Override
    public double aumentaStipendio(double in_aumento) {
        stipendio+=in_aumento;
        return stipendio;
    }

    private String nome;
    private String cognome;
    private double stipendio;

}
