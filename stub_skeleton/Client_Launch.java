package stub_skeleton;

import socket_test.test.Record;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Logger;

import static java.lang.System.exit;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 26/10/14
 * Time: 15:25
 */
public class Client_Launch {

    private static final Logger log = Logger.getLogger(Client_Launch.class.getName());
    private static  Socket client_dns_socket = null;

    public static void main (String[] args){



        try {
            Socket sock = new Socket("localhost", 60000);
            log.info("Connessione a server dns");
            client_dns_socket = sock;

            // GETTING IP OF HOST xserver CONTACTING THE DNS SERVER
            DNS_Record dns_record = new DNS_Record("xserver",null,0);
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            log.info("inviando richiesta al server dns per host xserver");
            out.writeObject(dns_record);
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            DNS_Record in_dns = (DNS_Record) in.readObject();
            sock.close();
            ///////////////////////////////////////////////////////

            if (in_dns.getHost() == null){
                log.info("No record found in dns server");
                exit(-1);
            } else {

               Impiegato i = new Stub(in_dns.getIp(),in_dns.getPorta());
               log.info(i.getCognome());
               log.info(i.getNome());
               log.info(i.getStipendio() + "");
               log.info(i.aumentaStipendio(400) + "");
               log.info(i.getStipendio() + "");


            }




        } catch (IOException e) {
            log.info("Server DNS Irragiungibile");
            //e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (client_dns_socket!= null)
                try {
                    client_dns_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

    }



}
