package stub_skeleton;

import socket_test.test.Record;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 21/10/14
 * Time: 12:31
 */
public class Stub  implements Impiegato{

    private static final Logger log = Logger.getLogger(Stub.class.getName());

    public Stub (String in_ip, int in_port){
        server_ip = in_ip;
        port = in_port;
    }

    @Override
    public String getNome() {
        try {
            Socket sock = new Socket(server_ip, port);
            saved_socket = sock;
            log.info("Connessione a " + server_ip + " " + port);
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            log.info("inviando getNome al server");
            out.writeObject(new String("getNome"));
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            String to_return = (String) in.readObject();
            sock.close();
            return to_return;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (saved_socket!=null)
                try {
                    saved_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return "";
    }


    @Override
    public String getCognome() {
        try {
            Socket sock = new Socket(server_ip, port);
            saved_socket = sock;
            log.info("Connessione a " + server_ip + " " + port);
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            log.info("inviando getCognome al server");
            out.writeObject(new String("getCognome"));
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            String to_return = (String) in.readObject();
            sock.close();
            return to_return;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if (saved_socket!=null)
                try {
                    saved_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return "";
    }

    @Override
    public double getStipendio() {
        try {
            Socket sock = new Socket(server_ip, port);
            saved_socket = sock;
            log.info("Connessione a " + server_ip + " " + port);
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            log.info("inviando getStipendio al server");
            out.writeObject(new String("getStipendio"));
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            double to_return = in.readDouble();
            sock.close();
            return to_return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (saved_socket!=null)
                try {
                    saved_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return -1;
    }

    @Override
    public double aumentaStipendio(double in_aumento) {
        try {
            Socket sock = new Socket(server_ip, port);
            saved_socket = sock;
            log.info("Connessione a " + server_ip + " " + port);
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            log.info("inviando aumentaStipendio al server");
            out.writeObject(new String("aumentaStipendio"));
            out.flush();
            out.writeDouble(in_aumento);
            out.flush();
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
            double to_return = in.readDouble();
            sock.close();
            return to_return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (saved_socket!=null)
                try {
                    saved_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return -1;

    }

    private String server_ip;
    private int port;
    private Socket saved_socket= null;
}
