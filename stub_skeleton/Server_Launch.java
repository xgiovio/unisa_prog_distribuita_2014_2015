package stub_skeleton;

import socket_test.test.Record;

import java.util.HashMap;

/**
 * Created with xgiovio.macbookair.
 * User: xgiovio
 * Date: 26/10/14
 * Time: 15:25
 */
public class Server_Launch {

    public static void main (String[] args){

        Impiegato i = new Impiegato_Server_Object("Mario","Rossi",2000);
        final HashMap<String,Impiegato_Server_Object> database = new HashMap<String, Impiegato_Server_Object>();
        Skeleton s = new Skeleton(i,database);
        Thread skeleton_thread = new Thread(s);
        skeleton_thread.start();

    }



}
