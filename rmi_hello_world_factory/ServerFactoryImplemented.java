package rmi_hello_world_factory;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 30/11/2014.
 */
public class ServerFactoryImplemented extends UnicastRemoteObject implements ServerFactory {

    private static final long serialVersionUID = 2025591618699941250L;


    public ServerFactoryImplemented() throws RemoteException {

    }


    @Override
    public Hello request_hello_object(String nationality, String nome) throws RemoteException {
        if (nationality.equals("Italy")){
            return new HelloImplementedIt(nome);
        }else if (nationality.equals("France")){
            return new HelloImplementedFr(nome);
        }else {
            return new HelloImplementedEn(nome);
        }
    }
}
