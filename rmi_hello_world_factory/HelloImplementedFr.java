package rmi_hello_world_factory;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 01/12/2014.
 */
public class HelloImplementedFr extends UnicastRemoteObject implements Hello {


    private static final long serialVersionUID = 2025591618699941248L;

    public HelloImplementedFr(String in) throws RemoteException {
        nome=in;
    }

    @Override
    public String sayhello() throws RemoteException {
        return "Bonjour " + nome;
    }
    private String nome;
}
