package rmi_hello_world_factory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Giovanni on 30/11/2014.
 */
public class LaunchServer {


    public static void main (String[] args){

        System.setSecurityManager(new SecurityManager());

        try {
            ServerFactory f = new ServerFactoryImplemented();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("Factory",f);


        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


}
