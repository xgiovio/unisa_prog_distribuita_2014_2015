package rmi_hello_world_factory;

import com.sun.corba.se.spi.activation.Server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 01/12/2014.
 */
public class LaunchClient {


    public static void main (String[] args){

        System.setSecurityManager(new SecurityManager());

        try {
            ServerFactory f = (ServerFactory) Naming.lookup("rmi://localhost/Factory");
            Hello h = f.request_hello_object("Italy","Mario");
            System.out.println(h.sayhello());

            h = f.request_hello_object("France","Mario");
            System.out.println(h.sayhello());

            h = f.request_hello_object("Germany","Mario");
            System.out.println(h.sayhello());


        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }



}
