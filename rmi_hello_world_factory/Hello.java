package rmi_hello_world_factory;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 30/11/2014.
 */
public interface Hello extends Remote {
    String sayhello () throws RemoteException;
}
