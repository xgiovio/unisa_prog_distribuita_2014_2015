package rmi_hello_world;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 09/11/2014.
 */
public class HelloWorldImpl extends UnicastRemoteObject implements HelloWorldInterface {

    private static final long  serialVersionUID = 5184834L;

    public HelloWorldImpl()throws RemoteException {

    }

    @Override
    public String stampa() throws RemoteException {
        return "HelloWorld";
    }
}
