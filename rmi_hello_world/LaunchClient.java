package rmi_hello_world;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 16/11/2014.
 */
public class LaunchClient {

    private static final Logger l = Logger.getLogger("Client");

    public static void main (String[] arg) {


        try {
            l.info("Cercando l'oggetto remoto");
            HelloWorldInterface o = (HelloWorldInterface) Naming.lookup("rmi://localhost/HW");
            l.info("Found");
            l.info(o.stampa());

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }


}
