package rmi_hello_world;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 09/11/2014.
 */
public interface HelloWorldInterface extends Remote {


    String stampa () throws RemoteException;

}
