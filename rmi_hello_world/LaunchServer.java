package rmi_hello_world;

import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 09/11/2014.
 */
public class LaunchServer {


    public static void main (String[] arg){

        Logger l =Logger.getLogger("hellowordlogger");
        System.setSecurityManager(new SecurityManager());

        try {
            Registry registry = LocateRegistry.getRegistry();
            HelloWorldImpl obj = new HelloWorldImpl();
            l.info("Creato oggetto");
            registry.rebind("HW", obj);
            l.info("Oggetto registrato");


        }
        catch (Exception e){
            e.printStackTrace();
        }



    }


}
