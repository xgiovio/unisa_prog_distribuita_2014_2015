package rmi_chat_p2p;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Created by Giovanni on 09/12/2014.
 */
public interface ClientInterface extends Remote {


    void speak (Message m) throws RemoteException;
    void register (ClientInterface c, String n) throws RemoteException;
    void unregister (ClientInterface c, String n) throws RemoteException;

}
