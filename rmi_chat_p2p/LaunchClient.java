package rmi_chat_p2p;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class LaunchClient {

    public  static Logger l = Logger.getLogger("p2p");
    public static void main (String[] args ) {

        String name = "default_name"; // or args[0]
        String registry_id_name = name + String.valueOf(Math.random());
        System.out.println("Name : " + name + " id :" + registry_id_name);

        try {
            ClientInterface ci = new Client();
            String[] nodes = Naming.list("rmi://localhost");
            l.info("List of connected nodes from registry : " + nodes.length );
            for (int i = 0 ; i < nodes.length;i++){
                l.info("Naming lookup of " + nodes[i] );
                ClientInterface node = (ClientInterface) Naming.lookup( nodes[i]);
                node.register(ci,name);
                ((Client)ci).getList().add(node);
            }
            Naming.rebind(registry_id_name,ci);


            BufferedReader i = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String cmd = command(i);
                if (!cmd.equals("quit")) {
                    ArrayList<ClientInterface> list = ((Client)ci).getList();
                    Iterator<ClientInterface> it = list.iterator();
                    for ( ;it.hasNext();){
                        it.next().speak(new Message(name,cmd));
                    }
                } else {
                    ArrayList<ClientInterface> list = ((Client)ci).getList();
                    Iterator<ClientInterface> it = list.iterator();
                    for ( ;it.hasNext();){
                        it.next().unregister(ci,name);
                    }
                    Naming.unbind(registry_id_name);
                    break;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
        finally {
            System.exit(0);
        }


    }

    private static String command (BufferedReader i){
        try {
            return i.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "quit";
    }


}
