package rmi_chat_p2p;



import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 * Created by Giovanni on 10/12/2014.
 */
public class Client extends UnicastRemoteObject implements ClientInterface {

    public Client() throws RemoteException {
        list = new ArrayList<ClientInterface>();
    }


    @Override
    public void speak(Message m) throws RemoteException {
        System.out.println(m.getFrom() +" : " + m.getM());
    }

    @Override
    public void register(ClientInterface c, String n) throws RemoteException {
            list.add(c);
        System.out.println(n + " has joined");

    }

    @Override
    public void unregister(ClientInterface c, String n) throws RemoteException {
            list.remove(c);
        System.out.println(n + " has quit");

    }

    public ArrayList<ClientInterface> getList() throws RemoteException{
        return list;
    }

    private ArrayList<ClientInterface> list;

}
