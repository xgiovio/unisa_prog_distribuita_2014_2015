package rmi_Awareness;



import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 01/12/2014.
 */
public interface call_me_back extends Remote{

    void notify (String in) throws RemoteException;

}
