package rmi_Awareness;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 01/12/2014.
 */
public interface register_to_server extends Remote {

        void register (call_me_back obj) throws RemoteException;
        void deregister (call_me_back obj) throws RemoteException;
}
