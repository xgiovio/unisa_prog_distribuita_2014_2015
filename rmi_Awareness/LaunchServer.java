package rmi_Awareness;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Giovanni on 01/12/2014.
 */
public class LaunchServer {

    public static void main (String[] args){

        try {
            HelloInterface h = new HelloImplemented();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("HelloServer",h);



        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

}
