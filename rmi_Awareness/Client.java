package rmi_Awareness;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 01/12/2014.
 */
public class Client extends UnicastRemoteObject implements call_me_back {

    public Client() throws RemoteException {
    }

    @Override
    public void notify(String in) throws RemoteException {
        System.out.println(in);
    }
}
