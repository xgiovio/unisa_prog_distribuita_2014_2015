package rmi_Awareness;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;


/**
 * Created by Giovanni on 01/12/2014.
 */
public class LaunchClient {

    public static void main (String[] args) {

        BufferedReader i = new BufferedReader(new InputStreamReader(System.in));

        register_to_server rs = null;
        call_me_back client = null;
        try {
            client = new Client();
            Object server_object = (Object) Naming.lookup("rmi://localhost/HelloServer");
            rs = (register_to_server) server_object;
            rs.register(client);
            ((HelloInterface) server_object).sayhello();

            System.out.println("Press enter to exit and deregister");
            i.readLine();

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.deregister(client);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }


    }


}
