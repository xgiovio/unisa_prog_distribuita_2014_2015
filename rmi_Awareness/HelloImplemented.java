package rmi_Awareness;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.Vector;

/**
 * Created by Giovanni on 01/12/2014.
 */
public class HelloImplemented extends UnicastRemoteObject implements HelloInterface, register_to_server {

    public HelloImplemented () throws RemoteException{
        v = new Vector<call_me_back>();
    }

    @Override
    public String sayhello() throws RemoteException {
        return "HI!";
    }

    @Override
    public synchronized void register(call_me_back obj) throws RemoteException {
        v.add(obj);
        Iterator<call_me_back> it = v.iterator();
        for (;it.hasNext();){
            it.next().notify("Un nuovo utente si e' registrato");
        }

    }

    @Override
    public synchronized void deregister(call_me_back obj) throws RemoteException {
        v.remove(obj);
        Iterator<call_me_back> it = v.iterator();
        for (;it.hasNext();){
            it.next().notify("Un  utente si e' cancellato ");
        }

    }

    private Vector<call_me_back> v;

}
